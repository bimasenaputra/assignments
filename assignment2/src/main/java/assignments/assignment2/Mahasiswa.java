// Class mahasiswa merupakan representasi dari mahasiswa pada sistem akademik
package assignments.assignment2;

public class Mahasiswa {
    // menyimpan mata kuliah yang diambil oleh mahasiswa
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    // menyimpan string yang berisi peringatan jika IRS ada yang bermasalah
    private String[] masalahIRS = new String[11];
    // banyak mata kuliah yang bermasalah ditambah masalah jika sks lebih dari 24
    private int jumlahMasalahIRS; 
    // total sks yang diambil mahasiswa, dihitung berdasarkan jumlah sks tiap matkul yang diambil
    private int totalSKS; 
    // banyak mata kuliah yang diambil
    private int mataKuliahDiambil;
    // nama mahasiswa
    private String nama; 
    // jurusan mahasiswa
    private String jurusan;
    // npm mahasiswa
    private long npm;

    // constructor untuk inisialisasi setiap member variable class nya
    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.totalSKS = 0;
        this.mataKuliahDiambil = 0;
        this.jumlahMasalahIRS = 0;
        long jurusan = npm/10000000000L%100;
        if (jurusan == 1) this.jurusan = "Ilmu Komputer";
        else this.jurusan = "Sistem Informasi";
    }

    // konversi dari nama jurusan sebenarnya ke kode jurusan tersebut
    private String getKodeJurusan() {
        return this.jurusan.equals("Ilmu Komputer") ? "IK" : "SI";
    }
    
    // menambahkan mata kuliah baru pada array mataKuliah
    // memberikan peringatan jika mata kuliah yang diambil sudah penuh, 
    // tidak boleh mengambil mata kuliah lagi, atau mata kuliah sudah pernah diambil
    public void addMatkul(MataKuliah mataKuliah){
        // mengecek apakah mata kuliah yang ingin diambil sudah pernah diambil
        boolean telahDiambil = false;
        for (int i = 0; i < this.mataKuliahDiambil; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                telahDiambil = true;
                break;
            }
        }
        if (telahDiambil) System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya.");
        else if (mataKuliah.getJumlahMahasiswaTerdaftar() == mataKuliah.getKapasitasMataKuliah()) {
            System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya.");
        } else {
            if (this.mataKuliahDiambil == 10) System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
            else {
                // jika memenuhi ketiga persyaratan di atas, masukkan mata kuliah tersebut ke array
                // lalu menambahkan mahasiswa ke daftar mahasiswa yang mengambil mata kuliah tersebut
                // dan mengecek apakah mata kuliah yang diambil menyebabkan masalah IRS
                this.mataKuliah[this.mataKuliahDiambil] = mataKuliah;
                mataKuliah.addMahasiswa(this);
                this.totalSKS += mataKuliah.getSKSMataKuliah();
                if (this.totalSKS > 24) {
                    this.masalahIRS[10] = "SKS yang Anda ambil lebih dari 24";
                    ++this.jumlahMasalahIRS;
                }
                String kodeJurusan = this.getKodeJurusan();
                if (!(mataKuliah.getKodeMataKuliah().equals("CS") || mataKuliah.getKodeMataKuliah().equals(kodeJurusan))) {
                    masalahIRS[this.mataKuliahDiambil] = "Mata Kuliah " + this.mataKuliah[this.mataKuliahDiambil] + " tidak dapat diambil jurusan " + kodeJurusan;
                    ++this.jumlahMasalahIRS;
                }
                ++this.mataKuliahDiambil;
            }
        }
    }

    // menghapus mata kuliah yang ingin di-drop
    public void dropMatkul(MataKuliah mataKuliah){
        // mencari index mata kuliah yang ingin di-drop, hapus mata kuliah tersebut jika pernah diambil
        int i = 0;
        while (i < this.mataKuliahDiambil) {
            if (this.mataKuliah[i] == mataKuliah) {
                this.mataKuliah[i] = null;
                break;
            }
            ++i;
        }

        // jika mata kuliah tersebut tidak pernah diambil, keluarkan peringatan
        if (i == this.mataKuliahDiambil) System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil.");
        else {
            // geser ke kiri mata kuliah lainnya di sisi kanan mata kuliah yang dihapus
            // lalu hapus mahasiswa dari daftar mahasiswa yang mengambil mata kuliah tersebut
            // dan mengecek apakah mata kuliah yang dihapus tersebut pernah menyebabkan masalah IRS
            for (int j = i; j < this.mataKuliahDiambil-1; j++) this.mataKuliah[j] = this.mataKuliah[j+1];
            this.mataKuliah[this.mataKuliahDiambil-1] = null;
            mataKuliah.dropMahasiswa(this);
            if (this.totalSKS > 24 && this.totalSKS - mataKuliah.getSKSMataKuliah() <= 24) {
                this.masalahIRS[10] = null;
                --this.jumlahMasalahIRS;
            }
            this.totalSKS -= mataKuliah.getSKSMataKuliah();
            String kodeJurusan = this.getKodeJurusan();
            if (!(mataKuliah.getKodeMataKuliah().equals("CS") || mataKuliah.getKodeMataKuliah().equals(kodeJurusan))) {
                masalahIRS[i] = null;
                --this.jumlahMasalahIRS;
            }
            --this.mataKuliahDiambil;
        }
    }

    // cetak semua masalah IRS jika ada. Jika tidak, cetak IRS tidak bermasalah
    public void cekIRS() {
        if (this.jumlahMasalahIRS == 0) System.out.println("IRS tidak bermasalah.");
        else {
            int counter = 1;
            if (this.masalahIRS[10] != null) {
                System.out.println(counter + ". " + this.masalahIRS[10]);
                ++counter;
            }
            for (int i = 0; i < 10; i++) {
                if (this.masalahIRS[i] != null) {
                    System.out.println(counter + ". " + this.masalahIRS[i]);
                    ++counter;
                }
            }
        }
    }

    // representasi string dari Mahasiswa yaitu berupa nama mahasiswa
    public String toString() {
        return this.nama;
    }

    // getter method untuk tiap member variable class-nya kecuali nama mahasiswa
    
    public long getNPM() {
        return this.npm;
    }

    public String getJurusan() {
        return this.jurusan;
    }

    public int getTotalSKS() {
        return this.totalSKS;
    }

    public int getMataKuliahDiambil() {
        return this.mataKuliahDiambil;
    }

    public MataKuliah[] getMataKuliah() {
        return this.mataKuliah;
    }

}

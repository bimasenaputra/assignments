// Class SistemAkademik adalah class utama untuk mensimulasikan administrasi akademik mahasiswa
package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    // kode perintah addMatkul
    private static final int ADD_MATKUL = 1; 
    // kode perintah dropMatkul
    private static final int DROP_MATKUL = 2;
    // kode perintah ringkasanMahasiswa
    private static final int RINGKASAN_MAHASISWA = 3;
    // kode perintah ringkasanMataKuliah
    private static final int RINGKASAN_MATAKULIAH = 4;
    // kode perintah keluar program
    private static final int KELUAR = 5; 
    // banyak mahasiswa yang terdaftar pada program
    private static int jumlahMahasiswa = 0;
    // banyak mata kuliah yang terdaftar pada program
    private static int jumlahMataKuliah = 0;
    // daftar mahasiswa yang terdaftar pada program
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    // daftar mata kuliah yang terdaftar pada program
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    // scanner untuk menerima dan mengembalikan input dari terminal
    private Scanner input = new Scanner(System.in); 

    // mencari mahasiswa yang terdaftar dengan npm (npm dijamin unik)
    private Mahasiswa getMahasiswa(long npm) {
        int indexMahasiswa = 0;
        for (int i = 0; i < jumlahMahasiswa; i++) {
            if (daftarMahasiswa[i].getNPM() == npm) {
                indexMahasiswa = i;
                break;
            }
        }
        return daftarMahasiswa[indexMahasiswa];
    }

    // mencari mata kuliah yang terdaftar dengan nama mata kuliah (nama mata kuliah dijamin unik)
    private MataKuliah getMataKuliah(String namaMataKuliah) {
        int indexMataKuliah = 0;
        for (int i = 0; i < jumlahMataKuliah; i++) {
            if (daftarMataKuliah[i].toString().equals(namaMataKuliah)) {
                indexMataKuliah = i;
                break;
            }
        }
        return daftarMataKuliah[indexMataKuliah];
    }

    // menu supaya mahasiswa bisa menambah mata kuliah
    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        // menambah mata kuliah yang ingin diambil mahasiswa
        for (int i = 0; i < banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            mahasiswa.addMatkul(mataKuliah);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    // menu supaya mahasiswa bisa drop mata kuliah
    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);

        if (mahasiswa.getMataKuliahDiambil() == 0) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
            return;
        }

        System.out.print("Banyaknya Matkul yang Di-drop: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang di-drop:");
        // drop mata kuliah yang ingin di-drop mahasiswa
        for(int i = 0; i < banyakMatkul; i++) {
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            mahasiswa.dropMatkul(mataKuliah);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    // menampilkan seluruh informasi mahasiswa
    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa mahasiswa = getMahasiswa(npm);
        MataKuliah[] mataKuliah = mahasiswa.getMataKuliah();

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mahasiswa);
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mahasiswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        if (mahasiswa.getMataKuliahDiambil() == 0) System.out.println("Belum ada mata kuliah yang diambil");
        else {
            for (int i = 0; i < mahasiswa.getMataKuliahDiambil(); i++) {
                System.out.println((i+1) + ". " + mataKuliah[i]);
            }
        }

        System.out.println("Total SKS: " + mahasiswa.getTotalSKS());
        System.out.println("Hasil Pengecekan IRS:");
        mahasiswa.cekIRS();
    }

    // menampilkan seluruh informasi mata kuliah
    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        Mahasiswa[] daftarMahasiswaMataKuliah = mataKuliah.getDaftarMahasiswa();

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + namaMataKuliah);
        System.out.println("Kode: " + mataKuliah.getKodeMataKuliah());
        System.out.println("SKS: " + mataKuliah.getSKSMataKuliah());
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswaTerdaftar());
        System.out.println("Kapasitas: " + mataKuliah.getKapasitasMataKuliah());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        if (mataKuliah.getJumlahMahasiswaTerdaftar() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        } else {
            for (int i = 0; i < mataKuliah.getJumlahMahasiswaTerdaftar(); i++) {
                System.out.println((i+1) + ". " + daftarMahasiswaMataKuliah[i]);
            }
        }
    }

    // menu awal untuk mengakses menu-menu lainnya
    private void daftarMenu() {
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }

    // pengaturan awal ketika program dijalankan
    // pengaturan berupa mendaftar mata kuliah dan mahasiswa ke program
    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        // mendaftarkan mata kuliah
        for (int i=0; i < banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            MataKuliah mataKuliah = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            daftarMataKuliah[i] = mataKuliah;
            ++jumlahMataKuliah;
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        // mendaftarkan mahasiswa
        for (int i = 0; i < banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            Mahasiswa mahasiswa = new Mahasiswa(dataMahasiswa[0], npm);
            daftarMahasiswa[i] = mahasiswa;
            ++jumlahMahasiswa;
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }

}

// Class MataKuliah adalah representasi dari mata kuliah pada sistem akademik
package assignments.assignment2;

public class MataKuliah {
    private String kode; // kode mata kuliah yang memuat informasi jurusan apa yang boleh mengambil mata kuliah
    private String nama; // nama mata kuliah
    private int sks; // bobot sks mata kuliah
    private int kapasitas; // banyak mahasiswa yang boleh mengambil mata kuliah
    private int jumlahMahasiswa; // banyak mahasiswa yang sedang mengambil mata kuliah
    private Mahasiswa[] daftarMahasiswa; // daftar mahasiswa yang sedang mengambil mata kuliah

    // constructor untuk inisialisasi setiap member variable class nya
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.jumlahMahasiswa = 0;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    // menambahkan mahasiswa yang ingin mengambil mata kuliah
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.daftarMahasiswa[this.jumlahMahasiswa] = mahasiswa;
        ++this.jumlahMahasiswa;   
    }

    // menghapus mahasiswa yang ingin drop mata kuliah
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // mendapatkan index mahasiswa yang ingin drop mata kuliah, hapus jika ada pada daftar
        int i = 0;
        while (i < this.jumlahMahasiswa) {
            if (this.daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = null;
                break;
            }
            ++i;
        }
        // geser ke kiri mahasiswa lainnya di sisi kanan mahasiswa yang dihapus
        for (int j = i; j < this.jumlahMahasiswa-1; j++) this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
        this.daftarMahasiswa[this.jumlahMahasiswa-1] = null;
        --this.jumlahMahasiswa;
    }

    // representasi string class MataKuliah yaitu nama mata kuliahnya
    public String toString() {
        return this.nama;
    }

    // getter method untuk semua member variable class-nya kecuali nama mata kuliah

    public String getKodeMataKuliah() {
        return this.kode;
    }

    public int getSKSMataKuliah() {
        return this.sks;
    }

    public int getKapasitasMataKuliah() {
        return this.kapasitas;
    }

    public int getJumlahMahasiswaTerdaftar() {
        return this.jumlahMahasiswa;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }
}

package assignments.assignment4.backend;

/**
 * Class: MataKuliah
 * -----------------
 * Class MataKuliah adalah representasi dari mata kuliah pada sistem akademik.
 */
public class MataKuliah {
    // kode mata kuliah yang memuat informasi jurusan apa yang boleh mengambil mata kuliah
    private String kode;
    // nama mata kuliah
    private String nama;
    // bobot sks mata kuliah
    private int sks;
    // banyak mahasiswa yang boleh mengambil mata kuliah
    private int kapasitas;
    // banyak mahasiswa yang sedang mengambil mata kuliah
    private int jumlahMahasiswa;
    // daftar mahasiswa yang sedang mengambil mata kuliah
    private Mahasiswa[] daftarMahasiswa;

    /**
     * Constructor untuk inisialisasi setiap member variable class-nya.
     * @param kode
     * @param nama
     * @param sks
     * @param kapasitas
     */
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.jumlahMahasiswa = 0;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    /**
     * Menambahkan mahasiswa yang ingin mengambil mata kuliah.
     * @param mahasiswa
     */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.daftarMahasiswa[this.jumlahMahasiswa] = mahasiswa;
        ++this.jumlahMahasiswa;   
    }

    /**
     * Menghapus mahasiswa yang ingin drop mata kuliah.
     * @param mahasiswa
     */
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // mendapatkan index mahasiswa yang ingin drop mata kuliah, hapus jika ada pada daftar
        int i = 0;
        while (i < this.jumlahMahasiswa) {
            if (this.daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = null;
                break;
            }
            ++i;
        }
        // geser ke kiri mahasiswa lainnya di sisi kanan mahasiswa yang dihapus
        for (int j = i; j < this.jumlahMahasiswa-1; j++) this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
        this.daftarMahasiswa[this.jumlahMahasiswa-1] = null;
        --this.jumlahMahasiswa;
    }

    /**
     * @return nama mata kuliah.
     */
    public String toString() {
        return this.nama;
    }

    /**
     * @return kode mata kuliah.
     */
    public String getKodeMataKuliah() {
        return this.kode;
    }

    /**
     * @return sks mata kuliah.
     */
    public int getSKSMataKuliah() {
        return this.sks;
    }

    /**
     * @return banyak mahasiswa yang boleh mengambil mata kuliah.
     */
    public int getKapasitasMataKuliah() {
        return this.kapasitas;
    }

    /**
     * @return mahasiswa yang sedang mengambil mata kuliah.
     */
    public int getJumlahMahasiswaTerdaftar() {
        return this.jumlahMahasiswa;
    }

    /**
     * @return Array berisi objek mahasiswa yang sedang mengambil mata kuliah.
     */
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }
}

package assignments.assignment4.backend;

/**
 * Class: Mahasiswa
 * ----------------
 * Class mahasiswa merupakan representasi dari mahasiswa pada sistem akademik.
 */

public class Mahasiswa {
    // menyimpan mata kuliah yang diambil oleh mahasiswa
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    // menyimpan string yang berisi peringatan jika IRS ada yang bermasalah
    private String[] masalahIRS = new String[11];
    // banyak mata kuliah yang bermasalah ditambah masalah jika sks lebih dari 24
    private int jumlahMasalahIRS; 
    // total sks yang diambil mahasiswa, dihitung berdasarkan jumlah sks tiap matkul yang diambil
    private int totalSKS; 
    // banyak mata kuliah yang diambil
    private int mataKuliahDiambil;
    // nama mahasiswa
    private String nama; 
    // jurusan mahasiswa
    private String jurusan;
    // npm mahasiswa
    private long npm;

    /**
     * Constructor untuk inisialisasi setiap member variable class-nya.
     * @param nama
     * @param npm
     */
    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.totalSKS = 0;
        this.mataKuliahDiambil = 0;
        this.jumlahMasalahIRS = 0;
        long jurusan = npm/10000000000L%100;
        if (jurusan == 1) this.jurusan = "Ilmu Komputer";
        else this.jurusan = "Sistem Informasi";
    }

    /**
     * Konversi dari nama jurusan sebenarnya ke kode jurusan tersebut.
     */
    private String getKodeJurusan() {
        return this.jurusan.equals("Ilmu Komputer") ? "IK" : "SI";
    }
    
    /**
     * Menambahkan mata kuliah baru pada array mataKuliah.
     * Memberikan peringatan jika mata kuliah yang diambil sudah penuh, 
     * tidak boleh mengambil mata kuliah lagi, atau mata kuliah sudah pernah diambil.
     * @param mataKuliah
     * @return
     */
    public String addMatkul(MataKuliah mataKuliah){
        // mengecek apakah mata kuliah yang ingin diambil sudah pernah diambil
        boolean telahDiambil = false;
        for (int i = 0; i < this.mataKuliahDiambil; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                telahDiambil = true;
                break;
            }
        }
        if (telahDiambil) return "[DITOLAK] " + mataKuliah + " telah diambil sebelumnya.";
        else if (mataKuliah.getJumlahMahasiswaTerdaftar() == mataKuliah.getKapasitasMataKuliah()) {
            return "[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya.";
        } else {
            if (this.mataKuliahDiambil == 10) return "[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.";
            else {
                // jika memenuhi ketiga persyaratan di atas, masukkan mata kuliah tersebut ke array
                // lalu menambahkan mahasiswa ke daftar mahasiswa yang mengambil mata kuliah tersebut
                // dan mengecek apakah mata kuliah yang diambil menyebabkan masalah IRS
                this.mataKuliah[this.mataKuliahDiambil] = mataKuliah;
                mataKuliah.addMahasiswa(this);
                this.totalSKS += mataKuliah.getSKSMataKuliah();
                if (this.totalSKS > 24 && this.masalahIRS[10] != null) {
                    this.masalahIRS[10] = "SKS yang Anda ambil lebih dari 24";
                    ++this.jumlahMasalahIRS;
                }
                String kodeJurusan = this.getKodeJurusan();
                if (!(mataKuliah.getKodeMataKuliah().equals("CS") || mataKuliah.getKodeMataKuliah().equals(kodeJurusan))) {
                    this.masalahIRS[this.mataKuliahDiambil] = "Mata Kuliah " + this.mataKuliah[this.mataKuliahDiambil] + " tidak dapat diambil jurusan " + kodeJurusan;
                    ++this.jumlahMasalahIRS;
                }
                ++this.mataKuliahDiambil;
                return "[BERHASIL] Silakan cek rekap untuk melihat hasil pengecekan IRS.";
            }
        }
    }

    /**
     * Menghapus mata kuliah yang ingin di-drop.
     * @param mataKuliah
     * @return
     */
    public String dropMatkul(MataKuliah mataKuliah){
        // mencari index mata kuliah yang ingin di-drop, hapus mata kuliah tersebut jika pernah diambil
        int i = 0;
        while (i < this.mataKuliahDiambil) {
            if (this.mataKuliah[i] == mataKuliah) {
                this.mataKuliah[i] = null;
                break;
            }
            ++i;
        }

        // jika mata kuliah tersebut tidak pernah diambil, keluarkan peringatan
        if (i == this.mataKuliahDiambil) return "[DITOLAK] " + mataKuliah + " belum pernah diambil.";
        else {
            // geser ke kiri mata kuliah lainnya di sisi kanan mata kuliah yang dihapus
            // lalu hapus mahasiswa dari daftar mahasiswa yang mengambil mata kuliah tersebut
            // dan mengecek apakah mata kuliah yang dihapus tersebut pernah menyebabkan masalah IRS
            for (int j = i; j < this.mataKuliahDiambil-1; j++) {
                this.mataKuliah[j] = this.mataKuliah[j+1];
                this.masalahIRS[j] = this.masalahIRS[j+1];
            }
            this.mataKuliah[this.mataKuliahDiambil-1] = null;
            this.masalahIRS[this.mataKuliahDiambil-1] = null;

            mataKuliah.dropMahasiswa(this);
            if (this.totalSKS > 24 && this.totalSKS - mataKuliah.getSKSMataKuliah() <= 24) {
                this.masalahIRS[10] = null;
                --this.jumlahMasalahIRS;
            }
            this.totalSKS -= mataKuliah.getSKSMataKuliah();
            String kodeJurusan = this.getKodeJurusan();
            if (!(mataKuliah.getKodeMataKuliah().equals("CS") || mataKuliah.getKodeMataKuliah().equals(kodeJurusan))) {
                this.masalahIRS[i] = null;
                --this.jumlahMasalahIRS;
            }
            --this.mataKuliahDiambil;
            return "[BERHASIL] Silakan cek rekap untuk melihat hasil pengecekan IRS.";
        }
    }

    /**
     * Cetak semua masalah IRS jika ada. Jika tidak, cetak IRS tidak bermasalah.
     * @return String array berisikan masalah IRS.
     */
    public String[] cekIRS() {
        String[] ret = new String[this.jumlahMasalahIRS];
        int i = 0;
        if (this.masalahIRS[10] != null) ret[i++] = this.masalahIRS[10];
        for (int j = 0; j < 10; j++) if (this.masalahIRS[j] != null) ret[i++] = this.masalahIRS[j];
        return ret;
    }

    /**
     * @return nama mahasiswa.
     */
    public String toString() {
        return this.nama;
    }
    
    /**
     * @return NPM mahasiswa.
     */
    public long getNPM() {
        return this.npm;
    }

    /**
     * @return jurusan mahasiswa.
     */
    public String getJurusan() {
        return this.jurusan;
    }

    /**
     * @return total SKS mahasiswa.
     */
    public int getTotalSKS() {
        return this.totalSKS;
    }

    /**
     * @return banyak mata kuliah yang diambil.
     */
    public int getMataKuliahDiambil() {
        return this.mataKuliahDiambil;
    }

    /**
     * @return Array berisikan objek mata kuliah yang diambil.
     */
    public MataKuliah[] getMataKuliah() {
        return this.mataKuliah;
    }

    /**
     * @return jumlah masalah IRS.
     */
    public int getBanyakMasalahIRS() {
        return this.jumlahMasalahIRS;
    }
}

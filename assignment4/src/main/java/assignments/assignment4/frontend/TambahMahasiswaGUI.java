package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: TambahMahasiswaGUI
 * -------------------------
 * Class TambahMahasiswaGUI merupakan halaman untuk menambahkan (mendaftarkan) mahasiswa ke
 * daftar mahasiswa yang ada.
 */
public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        Color BRIGHT_GRAY = new Color(250, 250, 250);

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel namaLabel = new JLabel("Nama:");
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextField namaField = new JTextField();
        namaField.setMaximumSize(new Dimension(225, 20));
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel NPMLabel = new JLabel("NPM:");
        NPMLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextField NPMField = new JTextField();
        NPMField.setMaximumSize(new Dimension(225, 20));
        NPMField.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton addButton = new JButton("Tambahkan");
        addButton.setBackground(new Color(156, 208, 35));
        addButton.setForeground(BRIGHT_GRAY);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String namaInput = namaField.getText();
                String NPMInput = NPMField.getText();
                if (namaInput.length() == 0 || NPMInput.length() == 0) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else {
                    boolean dupExists = false;
                    for (int i = 0; i < daftarMahasiswa.size(); ++i) {
                        if (String.valueOf(daftarMahasiswa.get(i).getNPM()).equals(NPMInput)) {
                            dupExists = true;
                            break;
                        }
                    }
                    if (dupExists) {
                        JOptionPane.showMessageDialog(frame, String.format("NPM %s sudah pernah ditambahkan sebelumnya", NPMInput));
                    } else {
                        daftarMahasiswa.add(new Mahasiswa(namaInput, Long.parseLong(NPMInput)));
                        JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %s-%s berhasil ditambahkan", NPMInput, namaInput));
                    }
                }
            }
        });

        JButton returnButton = new JButton("Kembali");
        returnButton.setBackground(new Color(130, 189, 209));
        returnButton.setForeground(BRIGHT_GRAY);
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 75)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(namaLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(namaField);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(NPMLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(NPMField);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(addButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(returnButton);
    }
    
}

package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: RingkasanMataKuliahGUI
 * -----------------------------
 * Class RingkasanMataKuliahGUI merupakan halaman untuk memasukan nama mata kuliah yang ingin dilihat
 * ringkasannya.
 */
public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        Color BRIGHT_GRAY = new Color(250, 250, 250);
        String[] matkulArr = new String[daftarMataKuliah.size()];
        
        for (int i = 0; i < daftarMataKuliah.size(); ++i) matkulArr[i] = daftarMataKuliah.get(i).toString();

        mergeSort(matkulArr, matkulArr.length);

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel matkulLabel = new JLabel("Pilih Nama Matkul");
        matkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JComboBox<String> matkulCB = new JComboBox<>(matkulArr);
        matkulCB.setMaximumSize(new Dimension(150, 20));

        JButton removeButton = new JButton("Lihat");
        removeButton.setBackground(new Color(156, 208, 35));
        removeButton.setForeground(BRIGHT_GRAY);
        removeButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (matkulCB.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else {
                    String matkulInput = String.valueOf(matkulCB.getSelectedItem());
                    frame.getContentPane().removeAll();
                    new DetailRingkasanMataKuliahGUI(frame, getMataKuliah(daftarMataKuliah, matkulInput), daftarMahasiswa, daftarMataKuliah);
                    frame.revalidate();
                    frame.repaint();
                }
            }
        });

        JButton returnButton = new JButton("Kembali");
        returnButton.setBackground(new Color(130, 189, 209));
        returnButton.setForeground(BRIGHT_GRAY);
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 125)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(matkulLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(matkulCB);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(removeButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(returnButton);
    }

    /**
     * Mendapatkan objek mata kuliah dengan nama sesuai argumen.
     * @param daftarMataKuliah
     * @param nama
     * @return objek mata kuliah.
     */
    private MataKuliah getMataKuliah(ArrayList<MataKuliah> daftarMataKuliah, String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.toString().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
    /**
     * Mengurutkan semua elemen string pada string array berdasarkan alfabet
     * menggunakan algoritma merge sort.
     * @param arr
     * @param len
     */
    public static void mergeSort(String[] arr, int len) {
        if (len >= 2) {
            int mid = len/2;
            String[] left = new String[mid];
            String[] right = new String[len - mid];

            for (int i = 0; i < mid; ++i) left[i] = arr[i];
            for (int i = mid; i < len; ++i) right[i - mid] = arr[i];

            mergeSort(left, mid);
            mergeSort(right, len - mid);

            int i = 0, j = 0, k = 0;

            while (i < mid && j < len - mid) {
                if (left[i].toLowerCase().compareTo(right[j].toLowerCase()) < 0) arr[k++] = left[i++]; 
                else arr[k++] = right[j++];
            }

            while (i < mid) arr[k++] = left[i++];
            while (j < len - mid) arr[k++] = right[j++];
        }
    }
}

package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: TambahIRSGUI
 * -------------------
 * Class TambahIRSGUI merupakan halaman untuk menambahkan IRS seorang mahasiswa.
 */
public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        Color BRIGHT_GRAY = new Color(250, 250, 250);
        String[] NPMArr = new String[daftarMahasiswa.size()];
        String[] matkulArr = new String[daftarMataKuliah.size()];
        
        for (int i = 0; i < daftarMahasiswa.size(); ++i) NPMArr[i] = String.valueOf(daftarMahasiswa.get(i).getNPM());
        for (int i = 0; i < daftarMataKuliah.size(); ++i) matkulArr[i] = daftarMataKuliah.get(i).toString();

        mergeSort(NPMArr, NPMArr.length);
        mergeSort(matkulArr, matkulArr.length);

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah IRS");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel NPMLabel = new JLabel("Pilih NPM");
        NPMLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JComboBox<String> NPMCB = new JComboBox<>(NPMArr);
        NPMCB.setMaximumSize(new Dimension(150, 20));

        JLabel matkulLabel = new JLabel("Pilih Nama Matkul");
        matkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JComboBox<String> matkulCB = new JComboBox<>(matkulArr);
        matkulCB.setMaximumSize(new Dimension(150, 20));

        JButton addButton = new JButton("Tambahkan");
        addButton.setBackground(new Color(156, 208, 35));
        addButton.setForeground(BRIGHT_GRAY);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (NPMCB.getSelectedItem() == null || matkulCB.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else {
                    String NPMInput = String.valueOf(NPMCB.getSelectedItem());
                    String matkulInput = String.valueOf(matkulCB.getSelectedItem());
                    JOptionPane.showMessageDialog(frame, getMahasiswa(daftarMahasiswa, Long.parseLong(NPMInput)).addMatkul(getMataKuliah(daftarMataKuliah, matkulInput)));
                }
            }
        });

        JButton returnButton = new JButton("Kembali");
        returnButton.setBackground(new Color(130, 189, 209));
        returnButton.setForeground(BRIGHT_GRAY);
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 75)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(NPMLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(NPMCB);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(matkulLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(matkulCB);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(addButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(returnButton);        
    }

    /**
     * Mendapatkan objek mata kuliah dengan nama sesuai argumen.
     * @param daftarMataKuliah
     * @param nama
     * @return objek mata kuliah.
     */
    private MataKuliah getMataKuliah(ArrayList<MataKuliah> daftarMataKuliah, String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.toString().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    /**
     * Mendapatkan objek mahasiswa dengan npm sesuai argumen.
     * @param daftarMahasiswa
     * @param npm
     * @return objek mahasiswa.
     */
    private Mahasiswa getMahasiswa(ArrayList<Mahasiswa> daftarMahasiswa, long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNPM() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

    /**
     * Mengurutkan semua elemen string pada string array berdasarkan alfabet
     * menggunakan algoritma merge sort.
     * @param arr
     * @param len
     */
    public static void mergeSort(String[] arr, int len) {
        if (len >= 2) {
            int mid = len/2;
            String[] left = new String[mid];
            String[] right = new String[len - mid];

            for (int i = 0; i < mid; ++i) left[i] = arr[i];
            for (int i = mid; i < len; ++i) right[i - mid] = arr[i];

            mergeSort(left, mid);
            mergeSort(right, len - mid);

            int i = 0, j = 0, k = 0;

            while (i < mid && j < len - mid) {
                if (left[i].toLowerCase().compareTo(right[j].toLowerCase()) < 0) arr[k++] = left[i++]; 
                else arr[k++] = right[j++];
            }

            while (i < mid) arr[k++] = left[i++];
            while (j < len - mid) arr[k++] = right[j++];
        }
    }
}

package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: HomeGUI
 * --------------
 * Class HomeGUI merupakan landing page dari program.
 */

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        Color LIGHT_GREEN = new Color(156, 208, 35);
        Color BRIGHT_GRAY = new Color(250, 250, 250);

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton addMahasiswaButton = new JButton("Tambah Mahasiswa");
        addMahasiswaButton.setBackground(LIGHT_GREEN);
        addMahasiswaButton.setForeground(BRIGHT_GRAY);
        addMahasiswaButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addMahasiswaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        JButton addMataKuliahButton = new JButton("Tambah Mata Kuliah");
        addMataKuliahButton.setBackground(LIGHT_GREEN);
        addMataKuliahButton.setForeground(BRIGHT_GRAY);
        addMataKuliahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addMataKuliahButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        JButton addIRSButton = new JButton("Tambah IRS");
        addIRSButton.setBackground(LIGHT_GREEN);
        addIRSButton.setForeground(BRIGHT_GRAY);
        addIRSButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addIRSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        JButton removeIRSButton = new JButton("Hapus IRS");
        removeIRSButton.setBackground(LIGHT_GREEN);
        removeIRSButton.setForeground(BRIGHT_GRAY);
        removeIRSButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        removeIRSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        JButton summaryMahasiswaButton = new JButton("Lihat Ringkasan Mahasiswa");
        summaryMahasiswaButton.setBackground(LIGHT_GREEN);
        summaryMahasiswaButton.setForeground(BRIGHT_GRAY);
        summaryMahasiswaButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        summaryMahasiswaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        JButton summaryMataKuliahButton = new JButton("Lihat Ringkasan Mata Kuliah");
        summaryMataKuliahButton.setBackground(LIGHT_GREEN);
        summaryMataKuliahButton.setForeground(BRIGHT_GRAY);
        summaryMataKuliahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        summaryMataKuliahButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 75)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(addMahasiswaButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(addMataKuliahButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(addIRSButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(removeIRSButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(summaryMahasiswaButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(summaryMataKuliahButton);
    }
}

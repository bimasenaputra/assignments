package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: DetailRingkasanMahasiswaGUI
 * ----------------------------------
 * Class DetailRingkasanMahasiswaGUI merupakan halaman untuk menampilkan ringkasan mahasiswa yang
 * NPM-nya dimasukkan pada halaman Ringkasan Mahasiswa.
 */
public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        String info = "<html><div style='text-align: center;'><p>Nama: " + mahasiswa.toString() + 
        "</p><p>NPM: " + mahasiswa.getNPM() + 
        "</p><p>Jurusan: " + mahasiswa.getJurusan() + 
        "</p><p>Daftar Mata Kuliah:</p><p><b>";

        if (mahasiswa.getMataKuliahDiambil() != 0) {
            MataKuliah[] mataKuliah = mahasiswa.getMataKuliah();
            for (int i = 0; i < mahasiswa.getMataKuliahDiambil(); ++i) {
                info += (i+1) + ". " + mataKuliah[i].toString() + "</b></p><p><b>";
            }
        } else {
            info += "Belum ada mata kuliah yang diambil.</b></p><p><b>";
        }

        info += "</b>Total SKS: " + mahasiswa.getTotalSKS() + "</p><p>Hasil Pengecekan IRS:</p><p><b>";
        
        if (mahasiswa.getBanyakMasalahIRS() != 0) {
            String[] masalahIRS = mahasiswa.cekIRS();
            System.out.println(masalahIRS.length);
            System.out.println(mahasiswa.getBanyakMasalahIRS());
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); ++i) {
                info += (i+1) + ". " + masalahIRS[i] + "</b></p><p><b>";
            }
        } else {
            info += "IRS tidak bermasalah.</b></p><p><b>";
        }

        info += "</b></p></div></html>";

        JLabel infoDisplay = new JLabel(info, SwingConstants.CENTER);
        infoDisplay.setMaximumSize(new Dimension(450, 450));
        infoDisplay.setFont(new Font("Sans Serif", Font.PLAIN, 12));;
        infoDisplay.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton returnButton = new JButton("Selesai");
        returnButton.setBackground(new Color(156, 208, 35));
        returnButton.setForeground(new Color(250, 250, 250));
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 75)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(infoDisplay);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(returnButton);
    }
}

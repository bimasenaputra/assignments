package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: DetailRingkasanMataKuliahGUI
 * -----------------------------------
 * Class DetailRingkasanMataKuliahGUI merupakan halaman untuk menampilkan ringkasan mata kuliah yang
 * namanya dimasukkan pada halaman Ringkasan Mata Kuliah.
 */
public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        String info = "<html><div style='text-align: center;'><p>Nama mata kuliah: " + mataKuliah.toString() +
                    "</p><p>Kode: " + mataKuliah.getKodeMataKuliah() +
                    "</p><p>SKS: " + mataKuliah.getSKSMataKuliah() +
                    "</p><p>Jumlah mahasiswa: " + mataKuliah.getJumlahMahasiswaTerdaftar() +
                    "</p><p>Kapasitas: " + mataKuliah.getKapasitasMataKuliah() +
                    "</p><p>Daftar Mahasiswa:</p><p><b>";

        if (mataKuliah.getJumlahMahasiswaTerdaftar() != 0) {
            Mahasiswa[] mahasiswa = mataKuliah.getDaftarMahasiswa();
            for (int i = 0; i < mataKuliah.getJumlahMahasiswaTerdaftar(); ++i) {
                info += (i+1) + ". " + mahasiswa[i].toString() + "</b></p><p><b>";
            }
        } else {
            info += "Belum ada mahasiswa yang mengambil mata kuliah ini.</b></p><p><b>";
        }

        info += "</b></p></div></html>";

        JLabel infoDisplay = new JLabel(info, SwingConstants.CENTER);
        infoDisplay.setMaximumSize(new Dimension(450, 450));
        infoDisplay.setFont(new Font("Sans Serif", Font.PLAIN, 12));;
        infoDisplay.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton returnButton = new JButton("Selesai");
        returnButton.setBackground(new Color(156, 208, 35));
        returnButton.setForeground(new Color(250, 250, 250));
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 75)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(infoDisplay);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(returnButton);
    }
}

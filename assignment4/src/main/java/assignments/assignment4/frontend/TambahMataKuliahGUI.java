package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: TambahMataKuliahGUI
 * --------------------------
 * Class TambahMataKuliahGUI merupakan halaman untuk menambahkan (mendaftarkan) mata kuliah ke
 * daftar mata kuliah yang ada.
 */
public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        Color BRIGHT_GRAY = new Color(250, 250, 250);

        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel kodeLabel = new JLabel("Kode Mata Kuliah:");
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextField kodeField = new JTextField();
        kodeField.setMaximumSize(new Dimension(225, 20));
        kodeField.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel namaLabel = new JLabel("Nama Mata Kuliah:");
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextField namaField = new JTextField();
        namaField.setMaximumSize(new Dimension(225, 20));
        namaField.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        JLabel SKSLabel = new JLabel("SKS:");
        SKSLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextField SKSField = new JTextField();
        SKSField.setMaximumSize(new Dimension(225, 20));
        SKSField.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel kapasitasLabel = new JLabel("Kapasitas:");
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JTextField kapasitasField = new JTextField();
        kapasitasField.setMaximumSize(new Dimension(225, 20));
        kapasitasField.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton addButton = new JButton("Tambahkan");
        addButton.setBackground(new Color(156, 208, 35));
        addButton.setForeground(BRIGHT_GRAY);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String kodeInput = kodeField.getText();
                String namaInput = namaField.getText();
                String SKSInput = SKSField.getText();
                String kapasitasInput = kapasitasField.getText();
                if (kodeInput.length() == 0 || namaInput.length() == 0 || SKSInput.length() == 0 || kapasitasInput.length() == 0) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else {
                    boolean dupExists = false;
                    for (int i = 0; i < daftarMataKuliah.size(); ++i) {
                        if (daftarMataKuliah.get(i).toString().equalsIgnoreCase(namaInput)) {
                            dupExists = true;
                            break;
                        }
                    }
                    if (dupExists) {
                        JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", namaInput));
                    } else {
                        daftarMataKuliah.add(new MataKuliah(kodeInput, namaInput, Integer.parseInt(SKSInput), Integer.parseInt(kapasitasInput)));
                        JOptionPane.showMessageDialog(frame, String.format("Mata kuliah %s berhasil ditambahkan", namaInput));
                    }
                }
            }
        });

        JButton returnButton = new JButton("Kembali");
        returnButton.setBackground(new Color(130, 189, 209));
        returnButton.setForeground(BRIGHT_GRAY);
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                frame.revalidate();
                frame.repaint();
            }
        });

        // Menambahkan komponen ke frame.
        frame.add(Box.createRigidArea(new Dimension(0, 50)));
        frame.add(titleLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(kodeLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(kodeField);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(namaLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(namaField);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(SKSLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(SKSField);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(kapasitasLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(kapasitasField);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(addButton);
        frame.add(Box.createRigidArea(new Dimension(0, 15)));
        frame.add(returnButton);
    }
    
}

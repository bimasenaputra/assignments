package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

/**
 * Class: SistemAkademik
 * ---------------------
 * Class SistemAkademik adalah main driver dari program Administrator - Sistem Akademik.
 */

public class SistemAkademik {
    
    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

/**
 * Class: SistemAkademikGUI
 * ------------------------
 * Class SistemAkademikGUI membuat frame dan menyimpan informasi utama
 * untuk panel GUI lainnya yang tersedia pada program Administrator - Sistem Akademik.
 */

class SistemAkademikGUI extends JFrame{
    // menyimpan objek mahasiswa yang terdaftar pada program.
    private static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    // menyimpan objek mata kuliah yang terdaftar pada program.
    private static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    // font untuk title.
    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 20);

    /**
     * Membuat frame sebesar 500 x 500 yang menampilkan home.
     */
    public SistemAkademikGUI(){

        // Membuat Frame
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setTitle("Administrator - Sistem Akademik");
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);
    }
}

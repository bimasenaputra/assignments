// ExtractNPM class is a class to extract certain information
// given a valid NPM.

package assignments.assignment1;
import java.lang.Math;
import java.util.Scanner;

public class ExtractNPM {

    // Helper function: return a to the power n
    // the algorithm is based on the fact that even = 2n, odd = 2n + 1.
    // a^odd = a * a^even = a^(even+1) and a^even = a^2n = (a*a)^n
    private static long pow(long a,long n) {
        long ret = 1L;
        while (n != 0) {
            if (n%2 == 1) ret *= a;
            a *= a; n /= 2;
        }
        return ret;
    }
    
    // Helper function: Get 14th digit in NPM based on given algorithm
    private static long getLastNPMDigit(long number) {
        long sumDigits = 0, copy = number;
        int length = (int)(Math.log10(number) + 1); // get number of digits
        // get sum digits
        for (int i = 0; i < length; ++i) {
            sumDigits += copy%10;
            copy /= 10;
        }
        // base case: if sum digits's length is one, return the number
        if (sumDigits < 10) return sumDigits;
        else {
            sumDigits = 0; // reset
            // helper number to stabilize the formula to get correct pointer to digit 
            // starting from the first digit in each iteration.  
            // since in each iteration the number's length is decremented by one
            int stabilizer = 0;
            if (length%2 == 1) sumDigits += number/(length/2); 
            // get the sum of multiplication of digits in symmetry, ie first digit times last digit
            for (int i = 1; i <= length/2; ++i) {
                sumDigits += ((number/pow(10, length-i-stabilizer))%10)*(number%10);
                number /= 10;
                ++stabilizer;
            }
            return getLastNPMDigit(sumDigits);
        }
    }

    // Validate NPM, return boolean
    public static boolean validate(long npm) {
        boolean validLength = (int)(Math.log10(npm) + 1) == 14; // check if number's length is 14
        if (!validLength) return false;
        boolean validEnrollmentYear = npm/1000000000000L >= 10; // check if first digit is not zero
        if (!validEnrollmentYear) return false;
        long major = npm/10000000000L%100;  // get 3rd and 4th digit then check if it's valid
        boolean validMajor = major == 1 || major == 2 || major == 3 || major == 11 || major == 12;
        if (!validMajor) return false;
        boolean validAge = (2000 + npm/1000000000000L) - npm/100L%10000 >= 15; // check if the person age is over 14
        if (!validAge) return false;
        boolean validLastDigit = getLastNPMDigit(npm/10) == npm%10; // check if last digit is valid
        if (!validLastDigit) return false;
        return true;
    }

    // Extract information from NPM, return string with given format
    public static String extract(long npm) {
        long majorDigit = npm/10000000000L%100; // get 3rd and 4th digit
        String day, month, major;
        // assign to a particular major
        if (majorDigit == 1) major = "Ilmu Komputer";
        else if (majorDigit == 2) major = "Sistem Informasi";
        else if (majorDigit == 3) major = "Teknologi Informasi";
        else if (majorDigit == 11) major = "Teknik Telekomunikasi";
        else major = "Teknik Elektro";
        // if 5th and 6th digit has length one add zero in front of the number
        if (npm/100000000L%100 >= 10) day = String.valueOf(npm/100000000L%100);
        else day = "0" + npm/100000000L%100;
        // if 7th and 8th digit has length one, add zero in front of the number
        if (npm/1000000L%100 >= 10) month = String.valueOf(npm/1000000L%100); 
        else month = "0" + npm/1000000L%100;
        return "Tahun masuk: 20" + npm/1000000000000L + 
               "\nJurusan: " + major + 
               "\nTanggal Lahir: " + day + "-"+ month  + "-" + npm/100L%10000;
    }

    // driver
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            // take input until input is negative
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            if (!validate(npm)) System.out.println("NPM tidak valid!");
            else System.out.println(extract(npm));
        }
        input.close();
    }
}
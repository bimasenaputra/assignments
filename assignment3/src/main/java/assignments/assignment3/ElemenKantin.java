/**
 * Class: ElemenKantin
 * -------------------
 * Class ElemenKantin merupakan salah satu ElemenFasilkom. Class ElemenKantin pasti memiliki tipe ElemenKantin dan
 * memiliki kemampuan unik untuk menjual makanan.
 */

package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    // Objek makanan yang dijual.
    private Makanan[] daftarMakanan = new Makanan[10];

    /**
     * Membuat objek elemen kantin baru jika diberikan nama.
     * @param nama
     */
    public ElemenKantin(String nama) {
        super("ElemenKantin", nama);
    }

    /**
     * Menambahkan makanan yang dijual jika sebelumnya tidak dijual.
     * Memberikan peringatan jika makanan yang ingin ditambah sudah pernah ditambah sebelumnya.
     * @param nama
     * @param harga
     */
    public void setMakanan(String nama, long harga) {
        int i = 0;
        while (i < 10 && this.daftarMakanan[i] != null) {
            if (this.daftarMakanan[i].toString().equalsIgnoreCase(nama)) {
                System.out.println("[DITOLAK] " + nama + " sudah pernah terdaftar");
                return;
            }
            ++i;
        }
        this.daftarMakanan[i] = new Makanan(nama, harga);
        System.out.println(this + " telah mendaftarkan makanan " + nama + " dengan harga " + harga);
    }

    /**
     * Mengembalikan makanan jika diberikan namaMakanan.
     * @param namaMakanan
     * @return Makanan
     */
    public Makanan getMakanan(String namaMakanan) {
        int i = 0;
        while (i < 10 && !(this.daftarMakanan[i].toString().equalsIgnoreCase(namaMakanan))) ++i;
        return this.daftarMakanan[i];
    }

    /**
     * Mencari apakah namaMakanan berada pada daftar menu makanan objek.
     * @param namaMakanan
     * @return true jika objek menjual namaMakanan. Selain itu, false.
     */
    public boolean menjual(String namaMakanan) {
        for (int i = 0; i < 10 && this.daftarMakanan[i] != null; ++i) {
            if (this.daftarMakanan[i].toString().equalsIgnoreCase(namaMakanan)) return true;
        }
        return false;
    }
}
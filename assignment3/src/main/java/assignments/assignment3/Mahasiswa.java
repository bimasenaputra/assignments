/**
 * Class: Mahasiswa
 * ----------------
 * Class Mahasiswa merupakan salah satu ElemenFasilkom. Class Mahasiswa pasti memiliki tipe Mahasiswa dan
 * memiliki kemampuan unik untuk mengambil suatu mata kuliah.
 */

package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {

    // Daftar mata kuliah yang diambil.
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    // NPM mahasiswa untuk menentukan tanggal lahir dan jurusan.
    private long npm;
    // Tanggal lahir yang dapat diketahui berdasarkan NPM.
    private String tanggalLahir;
    // Banyak mata kuliah yang diambil objek saat ini.
    private int mataKuliahDiambil;
    // Jurusan objek.
    private String jurusan;

    /**
     * Membuat objek Mahasiswa baru jika diberikan nama dan npm.
     * @param nama
     * @param npm
     */
    public Mahasiswa(String nama, long npm) {
        super("Mahasiswa", nama);
        this.npm = npm;
        this.jurusan = this.extractJurusan(this.npm);
        this.tanggalLahir = this.extractTanggalLahir(this.npm);
    }

    /**
     * Menambahkan mata kuliah baru pada objek Mahasiswa.
     * Memberikan peringatan jika mata kuliah yang diambil sudah penuh,
     * tidak boleh mengambil mata kuliah lagi, atau mata kuliah sudah pernah diambil.
     * @param mataKuliah
     */
    public void addMatkul(MataKuliah mataKuliah) {
        // mengecek apakah mata kuliah yang ingin diambil sudah pernah diambil
        for (int i = 0; i < this.mataKuliahDiambil; i++) {
            if (this.daftarMataKuliah[i] == mataKuliah) {
                System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya");
                return;
            }
        }
        if (mataKuliah.getJumlahMahasiswaTerdaftar() == mataKuliah.getKapasitasMataKuliah()) {
            System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya");
        } else {
            if (this.mataKuliahDiambil == 10) System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10");
            else {
                this.daftarMataKuliah[this.mataKuliahDiambil] = mataKuliah;
                mataKuliah.addMahasiswa(this);
                ++this.mataKuliahDiambil;
                System.out.println(this + " berhasil menambahkan mata kuliah " + mataKuliah);
            }
        }
    }

    /**
     * Menghapus mata kuliah yang ingin di-drop
     * @param mataKuliah
     */
    public void dropMatkul(MataKuliah mataKuliah) {
        // mencari index mata kuliah yang ingin di-drop, hapus mata kuliah tersebut jika pernah diambil
        int i = 0;
        while (i < this.mataKuliahDiambil) {
            if (this.daftarMataKuliah[i] == mataKuliah) {
                this.daftarMataKuliah[i] = null;
                break;
            }
            ++i;
        }

        // jika mata kuliah tersebut tidak pernah diambil, keluarkan peringatan
        if (i == this.mataKuliahDiambil) System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil");
        else {
            // geser ke kiri mata kuliah lainnya di sisi kanan mata kuliah yang dihapus
            // lalu hapus mahasiswa dari daftar mahasiswa yang mengambil mata kuliah tersebut
            for (int j = i; j < this.mataKuliahDiambil-1; j++) this.daftarMataKuliah[j] = this.daftarMataKuliah[j+1];
            this.daftarMataKuliah[this.mataKuliahDiambil-1] = null;
            mataKuliah.dropMahasiswa(this);
            --this.mataKuliahDiambil;
            System.out.println(this + " berhasil drop mata kuliah " + mataKuliah);
        }
    }

    /**
     * Mendapatkan tanggal lahir berdasarkan NPM.
     */
    private String extractTanggalLahir(long npm) {
        return npm/100000000L%100 + "-" + npm/1000000L%100 + "-" + npm/100L%10000;
    }

    /**
     * Mendapatkan jurusan berdasarkan NPM.
     * @param npm
     * @return
     */
    private String extractJurusan(long npm) {
        return npm/10000000000L%100 == 1 ? "Ilmu Komputer" : "Sistem Informasi";
    }

    /**
     * Mengembalikan tanggal lahir.
     * @return String tanggal lahir
     */
    public String getTanggalLahir() {
        return this.tanggalLahir;
    }

    /**
     * Mengembalikan jurusan.
     * @return String jurusan
     */
    public String getJurusan() {
        return this.jurusan;
    }

    /**
     * Mendapatkan daftar mata kuliah yang diambil objek Mahasiswa saat ini.
     * @return String yang sudah diformat berdasarkan mata kuliah yang diambil terlebih dahulu.
     */
    public String getMataKuliah() {
        if (this.mataKuliahDiambil == 0) return "Belum ada mata kuliah yang diambil\n";
        String listMatkul = "";
        for (int i = 0; i < this.mataKuliahDiambil; ++i) listMatkul += (i+1) + ". "+ this.daftarMataKuliah[i] + "\n"; 
        return listMatkul;
    }
}
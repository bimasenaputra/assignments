/**
 * Class: MataKuliah
 * -----------------
 * Class MataKuliah merupakan class penunjang untuk kemampuan unik class Dosen dan Mahasiswa.
 */

package assignments.assignment3;

class MataKuliah {

    // Nama mata kuliah.
    private String nama;
    // Jumlah maksimal mahasiswa yang dapat mengambil objek MataKuliah.
    private int kapasitas;
    // Dosen pengajar.
    private Dosen dosen;
    // Daftar objek Mahasiswa yang mengambil objek MataKuliah.
    private Mahasiswa[] daftarMahasiswa;
    // Jumlah mahasiswa saat ini yang mengambil objek MataKuliah.
    private int jumlahMahasiswa;

    /**
     * Membuat objek MataKuliah baru jika diberikan nama dan kapasitas.
     */
    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    /**
     * Menambahkan mahasiswa ke dalam daftar objek Mahasiswa 
     * yang sedang mengambil objek MataKuliah. 
     * @param mahasiswa
     */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.daftarMahasiswa[this.jumlahMahasiswa] = mahasiswa;
        ++this.jumlahMahasiswa;
    }

    /**
     * Menghapus mahasiswa dari daftar objek Mahasiswa
     * yang sedang mengambil objek MataKuliah.
     * @param mahasiswa
     */
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // mendapatkan index mahasiswa yang ingin drop mata kuliah, hapus jika ada pada daftar
        int i = 0;
        while (i < this.jumlahMahasiswa) {
            if (this.daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = null;
                break;
            }
            ++i;
        }
        // geser ke kiri mahasiswa lainnya di sisi kanan mahasiswa yang dihapus
        for (int j = i; j < this.jumlahMahasiswa-1; j++) this.daftarMahasiswa[j] = this.daftarMahasiswa[j+1];
        this.daftarMahasiswa[this.jumlahMahasiswa-1] = null;
        --this.jumlahMahasiswa;
    }

    /**
     * Medaftarkan objek Dosen sebagai dosen pengajar objek MataKuliah.
     * @param dosen
     */
    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    /**
     * Menghapus objek Dosen sebagai dosen pengajar objek MataKuliah.
     */
    public void dropDosen() {
        this.dosen = null;
    }

    /**
     * Representasi string objek MataKuliah.
     * @return nama mata kuliah
     */
    public String toString() {
        return this.nama;
    }

    /**
     * Mengecek apakah objek MataKuliah memiliki dosen pengajar.
     * @return true jika objek memiliki dosen pengajar. Selain itu, false.
     */
    public boolean memilikiDosenPengajar() {
        return this.dosen != null;
    }

    /**
     * Mengembalikan jumlah mahasiswa yang saat ini sedang mengambil objek MataKuliah
     * @return int JumlahMahasiswaTerdaftar
     */
    public int getJumlahMahasiswaTerdaftar() {
        return this.jumlahMahasiswa;
    }

    /**
     * Mengembalikan Jumlah maksimal mahasiswa yang dapat mengambil objek MataKuliah.
     * @return
     */
    public int getKapasitasMataKuliah() {
        return this.kapasitas;
    }

    /**
     * Mengembalikan dosen pengajar objek MataKuliah saat ini.
     * @return String dosen jika ada dosen pengajar. Selain itu, Belum ada.
     */
    public String getDosen() {
        if (this.dosen == null) return "Belum ada";
        return this.dosen.toString();
    }

    /**
     * Mengecek apakah objek Mahasiswa mengambil objek MataKuliah.
     * @param mahasiswa
     * @return true jika objek Mahasiswa mengambil objek MataKuliah. Selain itu, false.
     */
    public boolean adaMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < this.jumlahMahasiswa; ++i) {
            if (this.daftarMahasiswa[i] == mahasiswa) return true;
        }
        return false;
    }

    /**
     * Mendapatkan daftar mahasiswa yang mengambil objek MataKuliah saat ini.
     * @return String yang sudah diformat berdasarkan mahasiswa yang mengambil objek MataKuliah terlebih dahulu.
     */
    public String getMahasiswa() {
        if (this.jumlahMahasiswa == 0) return "Belum ada mahasiswa yang mengambil mata kuliah ini\n";
        String listMahasiswa = "";
        for (int i = 0; i < this.jumlahMahasiswa; ++i) listMahasiswa += (i+1) + ". " + this.daftarMahasiswa[i] + "\n";
        return listMahasiswa;
    }
}
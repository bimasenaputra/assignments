/**
 * Class: Main
 * -----------
 * Class Main merupakan class untuk menjalankan sebuah program mengenai kehidupan di fasilkom, 
 * yang mempunyai elemen-elemen yang ada di fasilkom seperti dosen, mahasiswa, dan elemen kantin. 
 * Selain itu, tiap elemen yang ada juga dapat melakukan interaksi satu sama lain.
 */

package assignments.assignment3;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    // Objek ElemenFasilkom yang terdaftar pada program.
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    // Objek MataKuliah yang terdaftar pada program.
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    // Total objek ElemenFasilkom yang terdaftar pada program.
    private static int totalMataKuliah = 0;
    // Total objek MataKuliah yang terdaftar pada program.
    private static int totalElemenFasilkom = 0; 

    /**
     * Mendaftarkan objek mahasiswa baru pada program.
     * @param nama
     * @param npm
     */
    private static void addMahasiswa(String nama, long npm) {
        if (totalElemenFasilkom == 100) return;
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        ++totalElemenFasilkom;
        System.out.println(nama + " berhasil ditambahkan");
    }

    /**
     * Mendaftarkan objek dosen baru pada program.
     * @param nama
     */
    private static void addDosen(String nama) {
        if (totalElemenFasilkom == 100) return;
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        ++totalElemenFasilkom;
        System.out.println(nama + " berhasil ditambahkan");
    }

    /**
     * Mendaftaftarkan objek elemen kantin baru pada program.
     * @param nama
     */
    private static void addElemenKantin(String nama) {
        if (totalElemenFasilkom == 100) return;
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        ++totalElemenFasilkom;
        System.out.println(nama + " berhasil ditambahkan");
    }

    /**
     * Mendapatkan objek elemen fasilkom pada program jika diberikan nama objek tersebut.
     * @param objek
     * @return ElemenFasilkom
     */
    private static ElemenFasilkom getElemenFasilkom(String objek) {
        int i = 0;
        while (i < totalElemenFasilkom && !(daftarElemenFasilkom[i].toString().equalsIgnoreCase(objek))) ++i;
        return daftarElemenFasilkom[i];
    }

    /**
     * Melakukan interaksi menyapa antar dua objek elemen fasilkom yang ada pada program.
     * @param objek1
     * @param objek2
     */
    private static void menyapa(String objek1, String objek2) {
        if (objek1.equalsIgnoreCase(objek2)) System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        else getElemenFasilkom(objek1).menyapa(getElemenFasilkom(objek2));
    }

    /**
     * Menambahkan objek makanan baru pada objek elemen kantin.
     * @param objek
     * @param namaMakanan
     * @param harga
     */
    private static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom penjual;
        if (!((penjual = getElemenFasilkom(objek)).getTipe().equals("ElemenKantin"))) System.out.println("[DITOLAK] " + objek + " bukan merupakan elemen kantin");
        else ((ElemenKantin) penjual).setMakanan(namaMakanan, harga);
    }

    /**
     * Melakukan interaksi membeli makanan antara objek1 sebagai pembeli dan objek2 sebagai penjual.
     * @param objek1
     * @param objek2
     * @param namaMakanan
     */
    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom penjual;
        if (!((penjual = getElemenFasilkom(objek2)).getTipe().equals("ElemenKantin"))) System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        else if (objek1.equalsIgnoreCase(objek2)) System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        else ElemenFasilkom.membeliMakanan(getElemenFasilkom(objek1), penjual, namaMakanan);
    }

    /**
     * Mendaftarkan objek mata kuliah baru pada program.
     * @param nama
     * @param kapasitas
     */
    private static void createMatkul(String nama, int kapasitas) {
        if (totalMataKuliah == 100) return;
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        ++totalMataKuliah;
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    /**
     * Mendapatkan objek mata kuliah pada program jika diberikan nama objek tersebut.
     * @param namaMataKuliah
     * @return
     */
    private static MataKuliah getMataKuliah(String namaMataKuliah) {
        int i = 0;
        while (i < totalMataKuliah && !(daftarMataKuliah[i].toString().equalsIgnoreCase(namaMataKuliah))) ++i;
        return daftarMataKuliah[i];
    }

    /**
     * Menambahkan objek mata kuliah pada program ke objek mahasiswa.
     * @param objek
     * @param namaMataKuliah
     */
    private static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemenFasilkom;
        if (!((elemenFasilkom = getElemenFasilkom(objek)).getTipe().equalsIgnoreCase("Mahasiswa"))) System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        else ((Mahasiswa) elemenFasilkom).addMatkul(getMataKuliah(namaMataKuliah));
    }

    /**
     * Menghapus objek mata kuliah pada program dari objek mahasiswa.
     * @param objek
     * @param namaMataKuliah
     */
    private static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemenFasilkom;
        if (!((elemenFasilkom = getElemenFasilkom(objek)).getTipe().equalsIgnoreCase("Mahasiswa"))) System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        else ((Mahasiswa) elemenFasilkom).dropMatkul(getMataKuliah(namaMataKuliah));
    }

    /**
     * Menambahkan objek mata kuliah pada program ke objek dosen. 
     * @param objek
     * @param namaMataKuliah
     */
    private static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemenFasilkom;
        if (!((elemenFasilkom = getElemenFasilkom(objek)).getTipe().equalsIgnoreCase("Dosen"))) System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        else ((Dosen) elemenFasilkom).mengajarMataKuliah(getMataKuliah(namaMataKuliah));
    }

    /**
     * Menghapus objek mata kuliah pada program dari objek dosen.
     * @param objek
     */
    private static void berhentiMengajar(String objek) {
        ElemenFasilkom elemenFasilkom;
        if (!((elemenFasilkom = getElemenFasilkom(objek)).getTipe().equalsIgnoreCase("Dosen"))) System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        else ((Dosen) elemenFasilkom).dropMataKuliah();
    }

    /**
     * Mencetak nama, tanggal lahir, jurusan, dan daftar mata kuliah yang diambil mahasiswa.
     * @param objek
     */
    private static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemenFasilkom;
        if (!((elemenFasilkom = getElemenFasilkom(objek)).getTipe().equalsIgnoreCase("Mahasiswa"))) System.out.println("[DITOLAK] " + objek + " bukan merupakan seorang mahasiswa");
        else System.out.print("Nama: " + elemenFasilkom + 
                            "\nTanggal lahir: " + ((Mahasiswa) elemenFasilkom).getTanggalLahir() +
                            "\nJurusan: " + ((Mahasiswa) elemenFasilkom).getJurusan() +
                            "\nDaftar Mata Kuliah:\n" + ((Mahasiswa) elemenFasilkom).getMataKuliah());
    }

    /**
     * Mencetak nama, jumlah mahasiswa, kapasitas, dosen pengajar, dan daftar mahasiswa yang mengambil objek mata kuliah.
     * @param namaMataKuliah
     */
    private static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        System.out.print("Nama mata kuliah: " + mataKuliah +
                       "\nJumlah mahasiswa: " + mataKuliah.getJumlahMahasiswaTerdaftar() +
                       "\nKapasitas: " + mataKuliah.getKapasitasMataKuliah() +
                       "\nDosen pengajar: " + mataKuliah.getDosen() +
                       "\nDaftar mahasiswa yang mengambil mata kuliah ini:\n" + mataKuliah.getMahasiswa());
    }

    /**
     * Melakukan pembaruan terhadap nilai friendship berdasarkan kegiatan sebuah objek elemen fasilkom
     * yang dilakukan hari ini lalu mencetak ringkasannya.
     */
    private static void nextDay() {
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate"); 
        for (int i = 0; i < totalElemenFasilkom; ++i) daftarElemenFasilkom[i].lazy();
        friendshipRanking();
    }

    /**
     * Mengurutkan elemen fasilkom berdasarkan besar friendship dan nama lalu mencetaknya.
     */
    private static void friendshipRanking() {
        Arrays.sort(daftarElemenFasilkom, 0, totalElemenFasilkom);
        for (int i = 0; i < totalElemenFasilkom; ++i) {
            System.out.println((i+1) + ". " + daftarElemenFasilkom[i] + "(" + daftarElemenFasilkom[i].getFriendship() + ")");
            daftarElemenFasilkom[i].resetMenyapa();
        }
    }

    /**
     * Mencetak semua elemen fasilkom beserta nilai friendship-nya pada akhir hari sebelumnya
     * tanpa memperhitungkan kegiatan yang dilakukan sebuah objek elemen fasilkom hari ini.
     */
    private static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    /** main program */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}
/**
 * Class: ElemenFasilkom
 * ---------------------
 * Class ElemenFasilkom merupakan class utama yang objek subclass-nya bisa melakukan berbagai macam interaksi 
 * yang disediakan pada class Main. Class ini sangat mengedepankan nilai friendship dan menyediakan 
 * fitur interaksi menyapa dan membeli makanan yang hanya melalui hal itu sebuah objek dari subclass 
 * ini bisa menambah nilai friendship-nya.
 */

package assignments.assignment3;

abstract class ElemenFasilkom implements Comparable<ElemenFasilkom> {
    
    // Tipe ElemenFasilkom dengan pilihan: Mahasiswa, Dosen, ElemenKantin
    private String tipe;
    // Nama objek.
    private String nama;
    // Nilai friendship pada akhir hari tertentu yang dihitung berdasarkan banyak objek lain yang disapa.
    private int friendship;
    // Banyak objek lain yang disapa pada akhir hari tertentu.
    private int banyakMenyapa;
    // Nilai friendship sementara pada awal sampai akhir hari tertentu.
    private int lazyFriendship;
    // Banyak objek lain yang disapa pada awal sampai akhir hari tertentu.
    private int lazyBanyakMenyapa;
    // Banyak objek yang dibuat.
    private static int totalElemenFasilkom;
    // Menyimpan objek lain yang sudah pernah disapa pada hari tertentu.
    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    /**
     * Constructor blueprint untuk subclass. 
     * Membuat objek ElemenFasilkom baru jika diberikan tipe dan nama.
     * @param tipe
     * @param nama
     */
    protected ElemenFasilkom(String tipe, String nama) {
        this.tipe = tipe;
        this.nama = nama;
        ++totalElemenFasilkom;
    }

    /**
     * Objek class ini melakukan interaksi menyapa dengan objek lain class ini.
     * Dengan aturan: (1) Suatu objek hanya bisa menyapa objek lain sekali dalam sehari.
     * (2) Jika objek merupakan mahasiswa sedangkan objek yang disapa adalah dosen 
     * atau sebaliknya, tingkatkan nilai friendship kedua objek sebesar dua.
     * Memberikan peringatan jika objek sudah pernah menyapa objek lain di hari yang sama.
     * @param elemenFasilkom
     */
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        int i = 0;
        while (i < 100 && this.telahMenyapa[i] != null) {
            if (this.telahMenyapa[i] == elemenFasilkom) {
                System.out.println("[DITOLAK] " + this + " telah menyapa " + elemenFasilkom + " hari ini");
                return;
            }
            ++i;
        }
        this.telahMenyapa[i] = elemenFasilkom;
        ++this.lazyBanyakMenyapa;
        elemenFasilkom.reverseMenyapa(this);
        if (this.tipe.equalsIgnoreCase("Dosen") && elemenFasilkom.getTipe().equalsIgnoreCase("Mahasiswa") || 
        this.tipe.equalsIgnoreCase("Mahasiswa") && elemenFasilkom.getTipe().equalsIgnoreCase("Dosen")) {
            Dosen dosen;
            Mahasiswa mahasiswa;
            if (this.tipe.equalsIgnoreCase("Dosen") && elemenFasilkom.getTipe().equalsIgnoreCase("Mahasiswa")) {
                dosen = (Dosen) this;
                mahasiswa = (Mahasiswa) elemenFasilkom;
            } else {
                dosen = (Dosen) elemenFasilkom;
                mahasiswa = (Mahasiswa) this;
            }
            MataKuliah mataKuliah = dosen.getMataKuliah();
            if (mataKuliah != null && mataKuliah.adaMahasiswa(mahasiswa)) {
                mahasiswa.increaseFriendship(2);
                dosen.increaseFriendship(2);
            }
        }
        System.out.println(this + " menyapa dengan " + elemenFasilkom);
    }

    /**
     * Menghapus semua objek yang pernah disapa.
     */
    public void resetMenyapa() {
        this.banyakMenyapa = 0;
        for (int i = 0; i < 100 && this.telahMenyapa[i] != null; ++i) this.telahMenyapa[i] = null;
    }

    /**
     * Objek class ini melakukan interaksi membeli makanan dengan objek lain class ini.
     * Penjual harus merupakan objek ElemenKantin.
     * Jika transaksi berhasil, tambah nilai friendship kedua objek sebesar satu.
     * Memberikan peringatan jika transaksi gagal.
     * @param pembeli
     * @param penjual
     * @param namaMakanan
     */
    public static void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        if (((ElemenKantin) penjual).menjual(namaMakanan)) {
            pembeli.increaseFriendship(1);
            penjual.increaseFriendship(1);
            System.out.println(pembeli + " berhasil membeli " + namaMakanan + " seharga " + ((ElemenKantin) penjual).getMakanan(namaMakanan).getHarga());
        } else System.out.println("[DITOLAK] " + penjual + " tidak menjual " + namaMakanan);
    }

    /**
     * Representasi string objek class ini.
     */
    public String toString() {
        return this.nama;
    }

    /**
     * Mengembalikan tipe class.
     * @return String tipe
     */
    public String getTipe() {
        return this.tipe;
    }

    /**
     * Mengembalikan nilai friendship.
     * @return int friendship
     */
    public int getFriendship() {
        return this.friendship;
    }

    /**
     * Menambah nilai friendship pada hari ini sebesar value.
     * @param value
     */
    public void increaseFriendship(int value) {
        this.lazyFriendship += value;
    }

    /**
     * Melakukan interaksi menyapa untuk pihak lainnya yang disapa.
     * @param o
     */
    public void reverseMenyapa(ElemenFasilkom o) {
        this.telahMenyapa[this.lazyBanyakMenyapa] = o;
        ++this.lazyBanyakMenyapa;
    }

    /**
     * Melakukan pembaruan yang ditunda pada banyakMenyapa dan friendship.
     * Lalu, menentukan perubahan nilai friendship-nya.
     */
    public void lazy() {
        this.banyakMenyapa += this.lazyBanyakMenyapa;
        this.friendship += this.lazyFriendship;
        this.friendship = this.banyakMenyapa >= Math.ceil((totalElemenFasilkom-1)/2.0) ? Math.min(this.friendship + 10, 100) : Math.max(0, this.friendship - 5);
        this.lazyBanyakMenyapa = this.lazyFriendship = 0;
    }

    /**
     * Operator overloading untuk operasi sorting pada objek subclass ini.
     * Mengembalikan 0 ketika objek memiliki nama yang sama dan nilai friendship sama
     * Mengembalikan <0 ketika nilai friendship objek lebih besar dari objek lain atau ketika
     * nilai friendship-nya sama, tetapi urutan namanya duluan.
     * Mengembalikan >0 ketika nilai friendship objek lebih kecil dari objek lain atau ketika
     * nilai friendship-nya sama, tetapi urutan namanya belakangan.
     */
    public int compareTo(ElemenFasilkom o) {
        int cmp = -Integer.compare(this.getFriendship(), o.getFriendship());
        if (cmp == 0) cmp = this.nama.compareTo(o.toString());
        return cmp;
    }
}
/**
 * Class: Dosen
 * ------------
 * Class Dosen merupakan salah satu ElemenFasilkom. Class Dosen pasti memiliki tipe Dosen dan
 * memiliki kemampuan unik untuk mengajar suatu mata kuliah.
 */

package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    // Mata kuliah yang diajar.
    private MataKuliah mataKuliah;

    /**
     * Membuat objek Dosen baru jika diberikan nama.
     * @param nama
     */
    public Dosen(String nama) {
        super("Dosen", nama);
    }

    /**
     * Mendaftarkan objek sebagai dosen pengajar mata kuliah.
     * Memberikan peringatan jika objek sedang mengajar mata kuliah yang ingin didaftarkan.
     * @param mataKuliah
     */
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (this.mataKuliah != null) System.out.println("[DITOLAK] " + this + " sudah mengajar mata kuliah " + this.mataKuliah); 
        else if (mataKuliah.memilikiDosenPengajar()) System.out.println("[DITOLAK] " + mataKuliah + " sudah memiliki dosen pengajar");
        else {
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.println(this + " mengajar mata kuliah " + mataKuliah);
        }
    }

    /**
     * Menghapus status objek sebagai dosen pengajar mata kuliah saat ini.
     * Memberikan peringatan jika objek tidak sedang mengajar mata kuliah apapun.
     */
    public void dropMataKuliah() {
        if (this.mataKuliah == null) System.out.println("[DITOLAK] " + this + " sedang tidak mengajar mata kuliah apapun");
        else {
            System.out.println(this + " berhenti mengajar " + this.mataKuliah);
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }

    /**
     * Mengembalikan mata kuliah yang diajar saat ini.
     * @return
     */
    public MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }
}
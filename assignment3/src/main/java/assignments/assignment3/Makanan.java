/**
 * Class: Makanan
 * --------------
 * Class Makanan merupakan class penunjang untuk kemampuan unik class ElemenKantin. 
 */

package assignments.assignment3;

class Makanan {

    // Nama makanan.
    private String nama;
    // Harga makanan.
    private long harga;

    /**
     * Membuat objek Makanan baru jika diberikan nama dan harga.
     * @param nama
     * @param harga
     */
    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    /**
     * Representasi string objek Makanan.
     * @return String nama makanan
     */
    public String toString() {
        return this.nama;
    }

    /**
     * Mengembalikan harga makanan.
     * @return int harga makanan
     */
    public long getHarga() {
        return this.harga;
    }
}